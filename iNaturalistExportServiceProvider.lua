--[[----------------------------------------------------------------------------
iNaturalistExportServiceProvider.lua
Publish service provider description for Lightroom iNaturalist uploader
--------------------------------------------------------------------------------
Rolf Lawrenz
 Copyright 2017 LawrenzCode
 All Rights Reserved.
------------------------------------------------------------------------------]]

	-- Lightroom SDK
local LrBinding = import 'LrBinding'
local LrDialogs = import 'LrDialogs'
local LrFileUtils = import 'LrFileUtils'
local LrPathUtils = import 'LrPathUtils'
local LrView = import 'LrView'

	-- Common shortcuts
local bind = LrView.bind
local share = LrView.share

	-- iNaturalist plug-in
require 'iNaturalistUser'
require 'iNaturalistAPI'
require 'iNaturalistPublishSupport'
require "Util.lua"

log("START iNaturalistExportServiceProvider")
--===========================================================================--
--[[
--- The <i>service definition script</i> for an export service provider defines the hooks
 -- that your plug-in uses to extend the behavior of Lightroom's Export features.
 -- The plug-in's <code>Info.lua</code> file identifies this script in the
 -- <code>LrExportServiceProvider</code> entry.
 -- <p>The service definition script should return a table that contains:
 --   <ul><li>A pair of functions that initialize and terminate your export service. </li>
 --	<li>Settings that you define for your export service.</li>
 --	<li>One or more items that define the desired customizations for the Export dialog.
 --	    These can restrict the built-in services offered by the dialog,
 --	    or customize the dialog by defining new sections. </li>
 --	<li> A function that defines the export operation to be performed
 --	     on rendered photos (required).</li> </ul>
 -- <p>The <code>FlickrExportServiceProvider.lua</code> file of the Flickr sample plug-in provides
 -- 	examples of and documentation for the hooks that a plug-in must provide in order to
 -- 	define an export service. Lightroom expects your plug-in to define the needed callbacks
 --	and properties with the required names and syntax. </p>
 -- <p>Unless otherwise noted, all of the hooks in this section are available to
 -- both Export and Publish service provider plug-ins. If your plug-in supports
 -- Lightroom's Publish feature, you should also read the API reference section
 -- <a href="SDK%20-%20Publish%20service%20provider.html">publish service provider</a>.</p>
 -- @module_type Plug-in provided

	module 'SDK - Export service provider' -- not actually executed, but suffices to trick LuaDocs

--]]

--iNaturalistExportItem = {}
--
--function iNaturalistExportItem.showModalDialog()
--  LrDialogs.message( "iNaturalistExportItem Selected", "Hello World!", "info" )
--end
--
--iNaturalistExportItem.showModalDialog()

local exportServiceProvider = {}

for name, value in pairs( iNaturalistPublishSupport ) do
	exportServiceProvider[ name ] = value
end

exportServiceProvider.supportsIncrementalPublish = 'only'


exportServiceProvider.allowFileFormats = { 'JPEG' }
exportServiceProvider.allowColorSpaces = { 'sRGB' }

exportServiceProvider.canExportVideo = false
exportServiceProvider.showSections = { 'fileNaming', 'imageSettings' }

function exportServiceProvider.startDialog( propertyTable )
  log("startDialog")
end

function exportServiceProvider.sectionsForTopOfDialog( f, propertyTable )
  log("sectionsForTopOfDialog")

  iNaturalistUser.setUserStatus(propertyTable)

  return {
    {
			title = LOC "$$$/iNaturalist/ExportDialog/Account=iNaturalist Account",

			f:row {
				spacing = f:control_spacing(),

				f:static_text {
					title = bind('accountStatus'),
					alignment = 'center',
					fill_horizontal = 1,
				},
			},

			f:row {
				f:push_button {
					title = 'Authorize',
					enabled = bind('authorizeButtonEnabled'),
					action = function()
					  iNaturalistUser.authorize( propertyTable )
					end,
				},
				f:static_text {
					title = "",
					fill_horizontal = 1,
				},
				f:push_button {
					title = 'Remove Authorization',
					enabled = bind('removeAuthorizationButtonEnabled'),
					action = function()
					  iNaturalistUser.removeAuthorization( propertyTable )
					end,
				},

			},
		},
	}
end

function exportServiceProvider.sectionsForBottomOfDialog( f, propertyTable )
  log("sectionsForBottomOfDialog")
  return {}
end

function exportServiceProvider.processRenderedPhotos( functionContext, exportContext )
  log("processRenderedPhotos")
  return {}
end

log("END iNaturalistExportServiceProvider")
return exportServiceProvider
