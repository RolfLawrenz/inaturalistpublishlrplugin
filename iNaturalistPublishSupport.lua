local LrDialogs = import 'LrDialogs'

local publishServiceProvider = {}

publishServiceProvider.small_icon = 'small_inaturalist.png'

function publishServiceProvider.didCreateNewPublishService( publishSettings, info )
end

function publishServiceProvider.didUpdatePublishService( publishSettings, info )
end

function publishServiceProvider.metadataThatTriggersRepublish( publishSettings )

	return {

--		default = false,
--		title = true,
--		caption = true,
--		keywords = true,
--		gps = true,
--		dateCreated = true,

		-- also (not used by Flickr sample plug-in):
			-- customMetadata = true,
			-- com.whoever.plugin_name.* = true,
			-- com.whoever.plugin_name.field_name = true,

	}

end

publishServiceProvider.supportsCustomSortOrder = true

function publishServiceProvider.validatePublishedCollectionName( proposedName )
	return true
end



iNaturalistPublishSupport = publishServiceProvider