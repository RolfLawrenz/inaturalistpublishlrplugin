--[[----------------------------------------------------------------------------

Rolf Lawrenz
 Copyright 2017 LawrenzCode
 All Rights Reserved.

--------------------------------------------------------------------------------

Info.lua
Publish photos to iNaturalist plug-in.

------------------------------------------------------------------------------]]

return {
	
	LrSdkVersion = 3.0,
	LrSdkMinimumVersion = 3.0, -- minimum SDK version required by this plug-in

	LrToolkitIdentifier = 'com.lawrenzcode.lightroom.inaturalist.publish',

--	LrExportMenuItems = {
--      title = "iNaturalist Publish",
--      file = "iNaturalistExportServiceProvider.lua",
--  },

	LrPluginName = LOC "$$$/iNaturalist/PluginName=iNaturalist Publish",

	LrExportServiceProvider = {
  		title = LOC "$$$/iNaturalist/iNaturalist-title=iNaturalist",
  		file = 'iNaturalistExportServiceProvider.lua',
  	},

  LrMetadataProvider = 'iNaturalistMetadataDefinition.lua',

	VERSION = { major=0, minor=0, revision=1, build=1, },

}
