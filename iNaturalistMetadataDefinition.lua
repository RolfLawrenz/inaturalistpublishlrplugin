--[[----------------------------------------------------------------------------

iNaturalistMetadataDefinition.lua
Custom metadata definition for iNaturalist publish plug-in
------------------------------------------------------------------------------]]

return {

	metadataFieldsForPhotos = {
	},

	schemaVersion = 2, -- must be a number, preferably a positive integer

}
