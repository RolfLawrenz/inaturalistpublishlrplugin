local LrDialogs = import 'LrDialogs'
local LrFunctionContext = import 'LrFunctionContext'
local LrTasks = import 'LrTasks'

require 'iNaturalistAPI'
require "Util.lua"


--============================================================================--

iNaturalistUser = {}

--------------------------------------------------------------------------------

local function isAccountValid()
  return false
end

function iNaturalistUser.setUserStatus(propertyTable)
  log("setUserStatus")

  propertyTable.validAccount = isAccountValid()

  if propertyTable.validAccount then
    propertyTable.accountStatus = "Authorized"
  else
    propertyTable.accountStatus = "Not authorized"
  end

  propertyTable.authorizeButtonEnabled = not propertyTable.validAccount
  propertyTable.removeAuthorizationButtonEnabled = propertyTable.validAccount
end

function iNaturalistUser.authorize( propertyTable )
  log("authorize")
  
  -- Inform user they will be going to iNaturalist Auth page  
  local authRequestDialogResult = LrDialogs.confirm(
    LOC "$$$/iNaturalist/AuthRequestDialog/Message=Lightroom needs your permission to upload images to iNaturalist.",
    LOC "$$$/iNaturalist/AuthRequestDialog/HelpText=If you click Authorize, you will be taken to a web page in your web browser where you can log in. When you're finished, return to Lightroom to complete the authorization.",
    LOC "$$$/iNaturalist/AuthRequestDialog/AuthButtonText=Authorize",
    LOC "$$$/LrDialogs/Cancel=Cancel" )

  log("Pressed "..authRequestDialogResult)

  if authRequestDialogResult == 'cancel' then
    return
  end

  propertyTable.accountStatus = LOC "$$$/iNaturalist/AccountStatus/WaitingForiNaturalist=Waiting for response from inaturalist.com..."

--  local frob = iNaturalistAPI.openAuthUrl()

  local waitForAuthDialogResult = LrDialogs.confirm(
    LOC "$$$/iNaturalist/WaitForAuthDialog/Message=Return to this window once you've authorized Lightroom on inaturalist.com.",
    LOC "$$$/iNaturalist/WaitForAuthDialog/HelpText=Once you've granted permission for Lightroom (in your web browser), click the Done button below.",
    LOC "$$$/iNaturalist/WaitForAuthDialog/DoneButtonText=Done",
    LOC "$$$/LrDialogs/Cancel=Cancel" )

  if waitForAuthDialogResult == 'cancel' then
    return
  end


end

function iNaturalistUser.removeAuthorization( propertyTable )
  log("removeAuthorization")
end